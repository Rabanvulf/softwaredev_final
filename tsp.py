import cPickle
import sys
import traceback

import bigGroup
import graphBit


def main(argv):
    num_nodes = 10      
    num_repetitions = 1  

    ## if list of arguments is at least 3 long and element zero is present 
    if len(argv) >= 3 and argv[0] and int(argv[0])>1:
        num_nodes = int(argv[0])   ## set number of nodes to input parameter
        if len(argv) >=4 and argv[3] and int(argv[3])>1:
            num_repetitions = int(argv[3])  ## set number of repetitions if argument is present

    ## if number of nodes is less than 10
    if num_nodes <= 10:
        num_ants = 20 ## number of ants
        num_iterations = 12 ## number of iterations 
    else:
        num_ants = 28
        num_iterations = 20

    loaded_data = cPickle.load(open(argv[1], "r")) ## load object from pickle file holding the cities and the connections between them
    cities = loaded_data[0] ## load cities
    city_matrix = loaded_data[1]  ## distance matrix

    ## if the length of the distance matrix is greater than the number of nodes reduce distance matrix to length of nodes
    if num_nodes < len(city_matrix):
        city_matrix = city_matrix[0:num_nodes]
        for i in range(0, num_nodes):
            city_matrix[i] = city_matrix[i][0:num_nodes]


    ## try block which throws an exception on failure
    try:
        graph = graphBit.GraphBit(num_nodes, city_matrix) ## create a graph with the nodes and distances between them
        best_path_vector = None 
        best_path_cost = sys.maxint ## best path cost set to highest possible value, lower is better
        
        ## loop over repetitions
        for i in range(0, num_repetitions):  
            print "Repetition %s" % i
            graph.reset_tau()   ## reset pheromone matrix
            workers = bigGroup.BigGroup(graph, num_ants, num_iterations) ## create a group of workers for a certain graph system with a given number of ants and iterations
            
            print "Colony Started"
            workers.start() ## workers start their traversal of the system - MAIN ROUTINE OF THE PROGRAM
            
            if workers.best_path_cost < best_path_cost:   ## if the current best path cost is less than the maximum path cost then print and save the found path length and cost
                best_path_vector = workers.best_path_vector
                best_path_cost = workers.best_path_cost
        
        ## when all iterations are done print out results
        print "\n------------------------------------------------------------"
        print "                     Results                                "
        print "------------------------------------------------------------"
        print "\nBest path = %s" % (best_path_vector,) ## best path in city indices
        
        city_vec = []
        for node in best_path_vector:
            print cities[node] + " ",   ## best path in city names
            city_vec.append(cities[node]) ## add the city names into output format data
        print "\nBest path cost = %s\n" % (best_path_cost,)
        
        results = [best_path_vector, city_vec, best_path_cost] ## collect results into one object
        cPickle.dump(results, open(argv[2], 'w+'))   ## write results into ouput file
    except Exception, e:
        print "exception: " + str(e)
        traceback.print_exc()

## entry point into program 
if __name__ == "__main__":
    main(sys.argv[1:])
