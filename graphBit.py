
## class contains methods and variables that have to do with the graph on which the ants traverse the system
class GraphBit:
    
    def __init__(self, num_nodes, delta_matrix, tau_matrix=None):  # **3**
        
        if len(delta_matrix) != num_nodes:     ## check if the distance matrix corresponds to the number of nodes used
            raise Exception("len(delta) != num_nodes")
        
        self.num_nodes = num_nodes
        self.delta_matrix = delta_matrix 
        
        if tau_matrix is None:     ## create a pheromone matrix
            self.tau_matrix = []
            for i in range(0, num_nodes):
                self.tau_matrix.append([0] * num_nodes)    ## create a list with elements of zero
    
    def delta(self, r, s):   ## return distance value at coords r,s
        return self.delta_matrix[r][s]
    
    def tau(self, r, s):     ## return pheromone value at coords r,s
        return self.tau_matrix[r][s]
    
    def etha(self, r, s):      ## return 1 over distance value at coords r,s
        return 1.0 / self.delta_matrix[r][s] 
    
    def update_tau(self, r, s, val):     ## set new value for tau at coords r,s
        self.tau_matrix[r][s] = val
    
    def reset_tau(self):   ## initializes the pheromone matrix to a base value
        avg = self.average_delta()
        self.tau0 = 1.0 / (self.num_nodes * 0.5 * avg)
        for r in range(0, self.num_nodes):
            for s in range(0, self.num_nodes):
                self.tau_matrix[r][s] = self.tau0      ## fill all elements of the tau matrix with tau0 values

    
    def average_delta(self):    ## find average of delta_matrix
        return self.average(self.delta_matrix)

    
    def average(self, matrix):  ## finds the average of a given matrix
        sum = 0
        for r in range(0, self.num_nodes):
            for s in range(0, self.num_nodes):
                sum += matrix[r][s]

        avg = sum / (self.num_nodes * self.num_nodes)
        return avg
