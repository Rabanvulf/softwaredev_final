import random
import sys
import work


class BigGroup:
    
    def __init__(self, graph, num_ants, num_iterations): 
        self.graph = graph
        self.num_ants = num_ants
        self.num_iterations = num_iterations
        self.Alpha = 0.1        ## parameter to control the influence of amount of pheromones deposited on a transition
        self.reset()
   
    ## reset the graph for each repetition
    def reset(self): 
        self.best_path_cost= sys.maxint       ## best path cost set a max possible value
        self.best_path_vector = None             
        self.best_path_matrix = None             
        self.last_best_path_iteration = 0
    
    ## start iterations
    def start(self):             
        self.ants = self.c_workers()    ## set up worker ants and assign them 
        self.iter_counter = 0

        while self.iter_counter < self.num_iterations:
            self.iteration()            ## iterations for all ants in a single network traversal 
            self.global_updating_rule()     ## refine global network with best result from ants
    
    ## iteration over all ants doing their runs
    def iteration(self):     
        self.avg_path_cost = 0
        self.ant_counter = 0
        self.iter_counter += 1
        
        for ant in self.ants:
            ant.run()

    ## when an ant finishes a path it calls an update to global stats
    def update(self, ant):         
        self.ant_counter += 1
        self.avg_path_cost += ant.path_cost     ## add the cost of the path of the ant to the average
        if ant.path_cost < self.best_path_cost:            ## if the cost is less than the current best, replace the path cost, path matrix and path vector
            self.best_path_cost= ant.path_cost
            self.best_path_matrix = ant.path_mat
            self.best_path_vector = ant.path_vec
            self.last_best_path_iteration = self.iter_counter       ## set current iteration counter to last_best_path_iteration 
            
        if self.ant_counter == len(self.ants):      ## once all ants have traversed the system print out a current best
            self.avg_path_cost /= len(self.ants)
            print "Best: %s, %s, %s, %s" % ( self.best_path_vector, self.best_path_cost, self.iter_counter, self.avg_path_cost,) ## left in for progress display, could be removed for better performance

     ## set up the worker ants to traverse the system
    def c_workers(self):   
        ants = []               
        for i in range(0, self.num_ants): 
            ant = work.Work(i, random.randint(0, self.graph.num_nodes - 1), self)       ## create a worker on a random starting node
            ants.append(ant)        
        return ants
    
     ## once all ants complete their run, the ant with the best path updates the vertices with extra pheromones
    def global_updating_rule(self):       
        evaporation = 0
        deposition = 0
        for r in range(0, self.graph.num_nodes):                ## loop over all vertices
            for s in range(0, self.graph.num_nodes):
                if r != s:                                      ## as long as the two indices are not the same
                    delt_tau = self.best_path_matrix[r][s] / self.best_path_cost
                    evaporation = (1 - self.Alpha) * self.graph.tau(r, s)
                    deposition = self.Alpha * delt_tau
                    self.graph.update_tau(r, s, evaporation + deposition)       ## update global pheromome matrix to give best path higher weight
