import math
import random


class Work():
    ## define all class parameters
    
    def __init__(self, ID, start_node, colony): 
        self.ID = ID
        self.start_node = start_node
        self.grouping = colony         
        self.curr_node = self.start_node
        self.graph = colony.graph
        self.path_vec = [self.start_node] ## vector to hold the path taken, initialised with the starting node
        self.path_cost = 0
        self.Beta = 1.0     ## parameter to control the attractiveness of a move
        self.Q0 = 0.5       ## constant for pheromone depositing
        self.Rho = 0.99     ## pheromone evaporation coefficient
        self.nodes_to_visit = []
        self.path_mat = []                          ## path matrix
        
       
       ## set up a list of nodes that remain to be visited 
        for i in range(0, self.graph.num_nodes):
            if i != self.start_node:                ## if the index is not the starting node
                self.nodes_to_visit.append(i)
        
        ## initialize path matrix to a size holding all nodes and set all element to zero
        for i in range(0, self.graph.num_nodes):
            self.path_mat.append([0] * self.graph.num_nodes)    

    
    ## run the main part of the algorithm
    def run(self): 
        graph = self.graph                                     ## relabel self.graph
        while self.nodes_to_visit:                  ## repeat as long as nodes to visit is not empty
            new_node = self.state_transition_rule(self.curr_node, graph)       ## update the new node according to the transition rule from the current node
            self.path_cost += graph.delta(self.curr_node, new_node)     ## add to the path cost
            self.path_vec.append(new_node)                              ## add the new node to the path vector
            self.path_mat[self.curr_node][new_node] = 1                 ## in the path matrix set the element in which the current node index and the new node index overlap to 1
            self.local_updating_rule(self.curr_node, new_node, graph)   ## update the local pheromone matrix
            self.curr_node = new_node                                   
        
        self.path_cost += graph.delta(self.path_vec[-1], self.path_vec[0])  ## add the cost of returning to the starting position 
        self.grouping.update(self)                                      ## call an update to the global statistics
        self.__init__(self.ID, self.start_node, self.grouping)          ## initialise to start for next run 


#    def prob_formula(self, node, curr_node, graph):
#        if self.Beta ==1.0:
#            return graph.tau_matrix[curr_node][node] / graph.delta_matrix[curr_node][node]     ## probabilistic formula
#        else:
#            return graph.tau_matrix[curr_node][node] * math.pow(graph.etha(curr_node, node), self.Beta)     ## probabilistic formula
    
    def prob_formula(self, node, curr_node, graph):  
        if self.Beta ==1.0:
            return graph.tau(curr_node, node) / graph.delta(curr_node, node)     ## probabilistic formula
        else:
            return graph.tau(curr_node, node) * math.pow(graph.etha(curr_node, node), self.Beta)     ## probabilistic formula
      
      
      
    def state_transition_rule(self, curr_node, graph):  
        q = random.random()                 
        max_node = -1
        if q < self.Q0:                     ## probabilistic check for transition to different city
            ## Exploitation section
            max_val = -1
            val = None
            
            ## find the node with the highest pheromone trail among the nodes to visit
            for node in self.nodes_to_visit:
                if graph.tau_matrix[curr_node][node] == 0:         ## check if pheromone trails between nodes exist
                    raise Exception("tau = 0")
                val = self.prob_formula(node, curr_node, graph)
                if val > max_val:       ## compare current pheromone probability with other values
                    max_val = val
                    max_node = node
                    
        else:                           ## otherwise choose a random city using the following probability distribution
            ## Exploration section
            sum = 0
            node = -1
            
            ## loop over all nodes left to visit 
            for node in self.nodes_to_visit:
                if graph.tau_matrix[curr_node][node] == 0:
                    raise Exception("tau = 0")          ## check if pheromone trails between nodes exist
                
                sum += self.prob_formula(node, curr_node, graph)  ## sum over pheromone values of unvisited cities
            
            if sum == 0:    ## catch errors
                raise Exception("sum = 0")
            
            avg = sum / len(self.nodes_to_visit)           ## average the sum over the number of unvisited cities
            
            ## calculate probability for an ant to travel to a city based on pheromone values
            for node in self.nodes_to_visit:
                p = self.prob_formula(node, curr_node, graph) 
                
                if p > avg:                     ## if the probability is larger than the average, set this to be the node to travel to.
                    max_node = node
                  
            if max_node == -1:                  ## correctness check
                max_node = node                 ## possibly redundant as this will only happen if max_node ==1 and node will equal -1 as well...
        
        if max_node < 0:
            raise Exception("max_node < 0")     ## error checking, only occurs when no new node has been found
        self.nodes_to_visit.remove(max_node)    ## remove visited node from unvisited list
        return max_node

    
    ## update the pheromone level on the just travelled upon path
    def local_updating_rule(self, curr_node, next_node, graph): 
        #Update the pheromones on the tau matrix to represent transitions of the ants
        val = (1 - self.Rho) * graph.tau(curr_node, next_node) + (self.Rho * graph.tau0)
        graph.update_tau(curr_node, next_node, val)
