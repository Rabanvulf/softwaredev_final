README

Software Development Coursework Part 3

Sorry about the mess with the commits, please ignore eveything that has to do with parts 1 and 2. This is a fork from the complete coursework and only contains the code for part 3.

I have not used a makefile as I'm still unfamiliar with them and didn't see the need for a python project of this small size.


To run the code please use the same format as before:
python tsp.py <number of cities to visit> <city data file> <outputfile>

with the example the same as before:
python tsp.py 12 citiesAndDistances.pickled output.pickled

if you wish to run the program multiple times with the new repetition feature please use this format:
python tsp.py <number of cities to visit> <city data file> <outputfile> <num of repetitions>

with the example:
python tsp.py 12 citiesAndDistances.pickled output.pickled 5


If you have any questions feel free to contact me on
s0935729@sms.ed.ac.uk
Thanks